package vendor;

// AAsst03

public class Base 
{
	int vin; // vehicle identity number
	String make; // company name: BMW, Honda, Mercedes, Nissan
	String model; // vehicle models, four for each make
	int year; // year of manufacture for each, one for each model
	long price; // price should include dollars and cents (payment plans are not included)
	String status; // Pending, Active, Sold, Canceled
	
	// https://dzone.com/articles/design-patterns-the-builder-pattern
	public Base(int vin, String make, String model, int year, long price, String status) 
	{
		super();
		this.vin = vin;
		this.make = make;
		this.model = model;
		this.year = year;
		this.price = price;
		this.status = status;
	}

	public int getVin() 
	{
		return vin;
	}
	public void setVin(int vin) 
	{
		this.vin = vin;
	}
	
	
	public String getMake() 
	{
		return make;
	}
	public void setMake(String make) 
	{
		this.make = make;
	}
	
	
	public String getModel() 
	{
		return model;
	}
	public void setModel(String model) 
	{
		this.model = model;
	}
	
	
	public int getYear() 
	{
		return year;
	}
	public void setYear(int year) 
	{
		this.year = year;
	}
	
	
	public long getPrice() 
	{
		return price;
	}
	public void setPrice(long price) 
	{
		this.price = price;
	}
	
	
	public String getStatus() 
	{
		return status;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}
	
	
	public String toString()
	{
		return " Vehicle Identity Number: " + vin + "\nVehicle Make: " + make + "\nVehicle Model: " + model + "\nVehicle Year: " + year + "\nAsking Price: " + price + "\nVehicle Status: " + status;
	}
}
