package vendor;

import java.util.HashMap;
import java.util.Scanner;

// AAsst03

public class BuildVIN 
{
	/*
	int vin; // vehicle identity number
	String make; // company name: BMW, Honda, Mercedes, Nissan
	String model; // vehicle models, four for each make
	int year; // year of manufacture for each, one for each model
	long price; // price should include dollars and cents (payment plans are not included)
	String status; // Pending, Active, Sold, Canceled
	*/
	public void create()
	{
		try
		{
			Scanner s = new Scanner(System.in);
			System.out.println("Enter Vehicle Identification Number");
			int vin = s.nextInt();
			
			if(!UI.custinfo.containsKey(vin))
			{
				System.out.println("Enter Vehicle Make");
				String make = s.next();
				System.out.println("Enter Vehicle Model");
				String model = s.next();
				System.out.println("Enter Vehicle Year");
				int year = s.nextInt();
				System.out.println("Enter Vehicle Price");
				long price = s.nextLong();
				System.out.println("Enter Vehicle Condition");
				String status = s.next();
				Base obj = new Base(vin, make, model, year, price, status);
				UI.custinfo.put(vin, obj);
				System.out.println("Profile Information Uploaded\n");
			}
			else
				System.out.println("This Vehicle IN already exists, please try again\n");
		}
		catch (Exception e)
		{
			System.out.println("Erroneous Entry, that is not a number\n");
		}
	}
	
	
	public void read()
	{
		try
		{
			Scanner s = new Scanner(System.in);
			System.out.println("Enter Vehicle IN to search");
			int vin = s.nextInt();
			if(!UI.custinfo.containsKey(vin))
			{
				System.out.println("There is no Vehicle with this IN, please try again\n");
			}
			else
				System.out.println(UI.custinfo.get(vin));
		}
		catch (Exception e)
		{
			System.out.println("Erroneous Entry, that is not a number\n");
		}
	}
	
	
	public void update()
	{
		try
		{
			Scanner s = new Scanner(System.in);
			System.out.println("Enter the Vehicle IN to edit its Listing");
			int vin = s.nextInt();
			if(!UI.custinfo.containsKey(vin))
			{
				System.out.println("There is no Vehicle with this IN, please try again\n");
			}
			else
			{
				System.out.println("Enter Vehicle Make");
				String make = s.next();
				System.out.println("Enter Vehicle Model");
				String model = s.next();
				System.out.println("Enter Vehicle Year");
				int year = s.nextInt();
				System.out.println("Enter Vehicle Price");
				long price = s.nextLong();
				System.out.println("Enter Vehicle Condition");
				String status = s.next();
				Base obj = new Base(vin, make, model, year, price, status);
				UI.custinfo.put(vin, obj);
				System.out.println("Listing Information Uploaded\n");
			}
		}
		catch (Exception e)
		{
			System.out.println("Erroneous Entry, that is not a number\n");
		}
	}
	
	
	public void delete()
	{
		try
		{
			Scanner s = new Scanner(System.in);
			System.out.println("Enter Vehicle IN to delete its Listing");
			int vin = s.nextInt();
			if(!UI.custinfo.containsKey(vin))
			{
				System.out.println("There is no Vehicle Listing with this IN\n");
			}
			else
			{
				UI.custinfo.remove(vin);
				System.out.println("Vehicle Listing has been deleted\n");
			}
		}
		catch (Exception e)
		{
			System.out.println("Erroneous Entry, that is not a number\n");
		}
	}
}
