package vendor;

import java.util.HashMap;
import java.util.Scanner;

// AAsst03

public class UI 
{
	static HashMap<Integer, Base> custinfo = new HashMap<>();
	/*
	int vin; // vehicle identity number
	String make; // company name: BMW, Honda, Mercedes, Nissan
	String model; // vehicle models, four for each make
	int year; // year of manufacture for each, one for each model
	long price; // price should include dollars and cents (payment plans are not included)
	String status; // Pending, Active, Sold, Canceled
	*/
	public static void main(String[] args)
	{
		System.out.println("Welcome");
		boolean flag = true;
		Scanner s = new Scanner(System.in);
		
		while(flag)
		{
			System.out.println("1) Create Listing");
			System.out.println("2) Read Listing");
			System.out.println("3) Update Listing");
			System.out.println("4) Delete Listing");
			System.out.println("5) Exit UI View");

			char option = s.next().charAt(0);
			
			switch(option)
			{
				case 49:
					new BuildVIN().create();
					break;
				case 50:
					new BuildVIN().read();
					break;
				case 51:
					new BuildVIN().update();
					break;
				case 52:
					new BuildVIN().delete();
					break;
				case 53:
				{
					System.out.println("Program Terminating, Exiting\n");
					flag = false;
					break;
				}
				default:
				{
					System.out.println("Invalid Entry, Please Try Again\n");
					break;
				}
			}
		}
	}
}
